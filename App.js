import * as React from 'react';
import {SafeAreaView, Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Login} from './src/screen/auth/Login';
import {Register} from './src/screen/auth/Register';
import {Account} from './src/screen/account/Account';
import {Home} from './src/screen/home/Home';
import {Order} from './src/screen/order/Order';
import {DetailOrder} from './src/screen/order/DetailOrder';
import {Cart} from './src/screen/cart/Cart';
import {DetailProduk} from './src/screen/produk/DetailProduk';
import {Pembayaran} from './src/screen/midtrans/Pembayaran';
import {Checkout} from './src/screen/checkout/Checkout';
import {ListAddress} from './src/screen/address/ListAddress';
import {AddAddress} from './src/screen/address/AddAddress';
const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

function SettingsScreen() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Settings!</Text>
    </View>
  );
}

export function TabHome() {
  return (
    <Tab.Navigator>
      <Tab.Screen
          name="Home"
          component={Home}
          options={{
            headerShown: false,
            tabBarIcon: ({focused}) => {
              return (
                <Icon
                  name="home"
                  color={focused ? '#39C5A3' : '#C9C9C9'}
                  size={25}
                />
              );
            },
            tabBarActiveTintColor: '#39C5A3',
            tabBarInactiveTintColor: '#C9C9C9',
          }}
        />
        {/* <Tab.Screen
          name="Cart"
          component={Cart}
          options={{
            headerShown: false,
            tabBarIcon: ({focused}) => {
              return (
                <Icon
                  name="cart-outline"
                  color={focused ? '#39C5A3' : '#C9C9C9'}
                  size={25}
                />
              );
            },
            tabBarActiveTintColor: '#39C5A3',
            tabBarInactiveTintColor: '#C9C9C9',
          }}
        /> */}
        <Tab.Screen
          name="Transaction"
          component={Order}
          options={{
            headerShown: false,
            tabBarIcon: ({focused}) => {
              return (
                <Icon
                  name="format-list-bulleted"
                  color={focused ? '#39C5A3' : '#C9C9C9'}
                  size={25}
                />
              );
            },
            tabBarActiveTintColor: '#39C5A3',
            tabBarInactiveTintColor: '#C9C9C9',
          }}
        />

        <Tab.Screen
          name="Account"
          component={Account}
          options={{
            headerShown: false,
            tabBarIcon: ({focused}) => {
              return (
                <Icon
                  name="account"
                  color={focused ? '#39C5A3' : '#C9C9C9'}
                  size={25}
                />
              );
            },
            tabBarActiveTintColor: '#39C5A3',
            tabBarInactiveTintColor: '#C9C9C9',
          }}
        />
    
    </Tab.Navigator>
  );
}

export function TabCart() {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Cart"
        component={Cart}
        options={{
          headerShown: false,
          tabBarIcon: ({focused}) => {
            return (
              <Icon
                name="cart-outline"
                color={focused ? '#39C5A3' : '#C9C9C9'}
                size={25}
              />
            );
          },
          tabBarActiveTintColor: '#39C5A3',
          tabBarInactiveTintColor: '#C9C9C9',
        }}
      />
    </Tab.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      {/* <Stack.Navigator >
        <Stack.Screen name="DetailProduk" component={DetailProduk} options={{headerShown: false}} />
      </Stack.Navigator> */}

      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Home" component={TabHome} />
        <Stack.Screen name="DetailProduk" component={DetailProduk} />
        <Stack.Screen name="Pembayaran" component={Pembayaran} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Checkout" component={Checkout} />
        <Stack.Screen name="ListAddress" component={ListAddress} />
        <Stack.Screen name="AddAddress" component={AddAddress} />
        <Stack.Screen name="DetailOrder" component={DetailOrder} />
        <Stack.Screen name="Cart" component={Cart} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
