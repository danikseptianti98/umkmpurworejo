import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import HeaderStack from '../../components/HeaderStack';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export class DetailProduk extends Component {
  constructor(props) {
    super(props);
    this.state = {
      detail: '',
      // url: 'http://192.168.1.92/restapi/',
      // url: 'http://192.168.0.108/restapi/',
      url: 'https://masterelectronic.000webhostapp.com/restapi/',


    };
  }

  // getDetail = async () => {
  //   const response = await axios.get(
  //     'http://192.168.1.92/restapi/restapi.php?function=get_detail_produk&id_produk='+28,
  //   );
  //   console.log('detail', response.data);
  //   this.setState({detail: response.data.data});
  // };

  componentDidMount() {
    // this.getDetail()
  }

  add_cart = async item => {
    const value = await AsyncStorage.getItem('id_user');
    const {detail} = this.props.route.params;

    let response = await axios.post(
      `${this.state.url}cart.php?function=add_to_cart`,
      {
        id_user: value,
        id_produk: detail.id_produk,
        qty: 1,
        total: detail.harga,
        //other data key value pairs
      },
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
    console.log('response', response);
    if (response.data.status === 1) {
      // console.log(response.data);
      // this.setState({status: 200});
      // await AsyncStorage.setItem('id_user', response.data.id_user)
      this.props.navigation.push('Cart');
    }
  };

  render() {
    const {detail} = this.props.route.params;

    console.log('detail produk', detail);
    return (
      <View style={styles.container}>
        <HeaderStack
          title={'Detail Produk'}
          onPress={() => this.props.navigation.goBack()}
        />
        <ScrollView>
          <View
            style={{
              backgroundColor: '#F1F1F1',
              width: wp(100),
              height: hp(40),
              // padding: wp(4),
            }}>
            <Image
              source={{
                uri: `${this.state.url}image/${detail.gambar}`,
              }}
              style={{
                width: wp(100),
                height: hp(40),
                resizeMode: 'contain',
                alignItems: 'center',
                justifyContent: 'center',
                alignSelf: 'center',
              }}
            />
          </View>
          {/* <View
            style={{
              marginHorizontal: wp(4),
              marginVertical: hp(2),
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#39C5A3',
                paddingVertical: wp(1),
                paddingHorizontal: wp(2),
                borderRadius: 5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{color: '#fff', fontWeight: 'bold'}}>
                Free Shipping
              </Text>
            </View>
            <TouchableOpacity
              style={{
                borderWidth: 0.3,
                borderColor: '#B0B0B0',
                padding: wp(2),
                borderRadius: wp(10),
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Icon name="heart" size={20} color={'#B0B0B0'} />
            </TouchableOpacity>
          </View> */}

          <View style={{marginHorizontal: wp(4), marginTop: hp(2)}}>
            <Text style={{color: '#292929', fontSize: 20, fontWeight: 'bold'}}>
              {detail.nama_produk}
            </Text>
            <Text
              style={{
                color: '#666666',
                fontSize: 14,
                lineHeight: 20,
                marginTop: hp(1),
              }}>
              {detail.deskripsi}
            </Text>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginVertical: hp(2.5),
              }}>
              {/* <Text
                style={{color: '#292929', fontSize: 24, fontWeight: 'bold'}}>
              Rp {detail.harga}
              </Text> */}
              {/* <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    width: wp(6),
                    height: wp(6),
                    borderRadius: wp(6),
                    backgroundColor: '#292929',
                    marginRight: wp(1),
                  }}
                />
                <View
                  style={{
                    width: wp(6),
                    height: wp(6),
                    borderRadius: wp(6),
                    backgroundColor: '#CCC7BB',
                    marginRight: wp(1),
                  }}
                />
                <View
                  style={{
                    width: wp(6),
                    height: wp(6),
                    borderRadius: wp(6),
                    backgroundColor: '#CFCFCF',
                  }}
                />
              </View> */}
            </View>
          </View>
          {/* <View
            style={{borderBottomWidth: 0.5, borderBottomColor: '#CFCFCF'}}
          /> */}
          {/* <View
            style={{
              paddingHorizontal: wp(4),
              paddingVertical: hp(2),
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View style={{flexDirection: 'row', marginRight: wp(4)}}>
              <Image
                source={{
                  uri: 'https://media.npr.org/assets/img/2022/11/08/ap22312071681283-0d9c328f69a7c7f15320e8750d6ea447532dff66-s1100-c50.jpg',
                }}
                style={{width: wp(6), height: wp(6), borderRadius: wp(6)}}
              />
              <Image
                source={{
                  uri: 'https://i.insider.com/5cb8b133b8342c1b45130629?width=700',
                }}
                style={{width: wp(6), height: wp(6), borderRadius: wp(6)}}
              />
              <Image
                source={{
                  uri: 'https://thumbs.dreamstime.com/b/human-emotion-reaction-concept-close-up-portrait-young-peopl-people-look-aside-hold-palm-hand-near-face-make-big-white-smile-127378211.jpg',
                }}
                style={{width: wp(6), height: wp(6), borderRadius: wp(6)}}
              />
            </View>
            <Text style={{fontWeight: 'bold'}}>8,200+ people pinned this</Text>
          </View> */}
          <View
            style={{borderBottomWidth: 0.5, borderBottomColor: '#CFCFCF'}}
          />
          {/* <View style={{marginHorizontal: wp(4), paddingVertical: hp(2)}}>
            <Text style={{fontWeight: 'bold'}}>
              Have a coupon code? enter here{' '}
            </Text>
            <TouchableOpacity
              style={{
                borderWidth: 0.7,
                borderColor: '#C9C9C9',
                paddingVertical: wp(4),
                paddingHorizontal: wp(4),
                borderRadius: 5,
                marginVertical: hp(2),
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{color: '#1E1E1E', fontWeight: 'bold'}}>AXDSFR</Text>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text
                  style={{
                    color: '#39C5A3',
                    fontWeight: 'bold',
                    marginRight: wp(2),
                  }}>
                  Available
                </Text>
                <Icon
                  name="checkbox-marked-circle"
                  color={'#39C5A3'}
                  size={18}
                />
              </View>
            </TouchableOpacity>
          </View> */}
        </ScrollView>
        <View style={{paddingVertical: hp(1.5)}}>
          <View style={{borderTopWidth: 0.5, borderTopColor: '#CFCFCF'}} />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              paddingHorizontal: wp(4),
              alignItems: 'center',
            }}>
            <Text style={{color: '#292929', fontSize: 24, fontWeight: 'bold'}}>
              Rp {detail.harga}
            </Text>

            <TouchableOpacity
              // onPress={() => this.props.navigation.navigate('Pembayaran')}
              onPress={() => this.add_cart()}
              style={{
                backgroundColor: '#39C5A3',
                paddingVertical: hp(1.5),
                width: wp(40),
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 7,
                marginTop: hp(1),
              }}>
              <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
                Add to cart
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default DetailProduk;
