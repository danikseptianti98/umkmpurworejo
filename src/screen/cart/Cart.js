import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from 'react-native';
import React, {Component} from 'react';
import HeaderStack from '../../components/HeaderStack';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import NotLogin from '../../components/NotLogin';

export class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id_user: null,
      cart: [],
      totalprice: 0,
      totalcart: 0,
      idUser: null,
      // url: 'http://192.168.1.92/restapi/',
      // url: 'http://192.168.0.108/restapi/',
      url: 'https://masterelectronic.000webhostapp.com/restapi/',


    };
  }

  async componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.getCart();
      this.getTotal();
    });
    const value = await AsyncStorage.getItem('id_user');
    if (value !== null) {
      this.setState({idUser: value});
    }

 
  }

  async componentWillUnmount() {
    this._unsubscribe();
  }
  getCart = async () => {
    const value = await AsyncStorage.getItem('id_user');
    const response = await axios.get(
      `${this.state.url}cart.php?function=get_list_cart&id_user=` + value,
    );
    console.log('cart', response.data);
    this.setState({cart: response.data.data});
  };
  getTotal = async () => {
    const value = await AsyncStorage.getItem('id_user');
    const response = await axios.get(
      `${this.state.url}cart.php?function=get_total_price&id_user=` + value,
    );
    // console.log('TOTAL', response.data);
    response.data.data.map(item => {
      // console.log('item',item.total);
      this.setState({
        totalprice: item,
        totalcart: item,
      });
    });
  };

  update_cart_increment = async item => {
    let response = await axios.post(
      `${this.state.url}cart.php?function=update_increment_cart&id_keranjang=` +
        item.id_keranjang,
      {
        qty: item.qty,
        price: item.harga,
        //other data key value pairs
      },
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
    console.log('response', response);
    if (response.data.status === 1) {
      this.getCart();
      this.getTotal();
    }
  };
  update_cart_decrement = async item => {
    let response = await axios.post(
      `${this.state.url}cart.php?function=update_decrement_cart&id_keranjang=` +
        item.id_keranjang,
      {
        qty: item.qty,
        price: item.harga,
        //other data key value pairs
      },
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
    console.log('response', response);
    if (response.data.status === 1) {
      this.getCart();
      this.getTotal();
    }
  };

  delete_cart = async item => {
    let response = await axios.delete(
      `${this.state.url}cart.php?function=delete_cart&id_keranjang=` +
        item.id_keranjang,
      {
        //other data key value pairs
      },
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
    console.log('response', response);
    if (response.data.status === 1) {
      this.getCart();
      this.getTotal();
    }
  };
  async getUser() {
    const value = await AsyncStorage.getItem('id_user');
    if (value !== null) {
      this.setState({idUser: value});
    }
  }

  render() {
    const {cart, totalprice, totalcart, idUser} = this.state;
    console.log('idUser cart', idUser);

    return (
      <View style={styles.container}>
        <HeaderStack
          title={'Keranjang'}
          onPress={() => this.props.navigation.goBack()}
        />
        {idUser !== null ? (
          <View style={{flex: 1}}>
            <View
              style={{borderBottomWidth: 0.5, borderBottomColor: '#CFCFCF'}}
            />

            <ScrollView>
              <View
                style={{borderBottomWidth: 0.5, borderBottomColor: '#CFCFCF'}}
              />
              {cart !== undefined &&
                cart.map(item => (
                  <View>
                    <View
                      style={{marginHorizontal: wp(5), marginVertical: hp(2)}}>
                      <View style={{flexDirection: 'row'}}>
                        <View
                          style={
                            {
                              // backgroundColor: '#F1F1F1',
                              // padding: wp(4),
                            }
                          }>
                          <Image
                            source={{
                              uri: `${this.state.url}image/${item.gambar}`,
                            }}
                            style={{
                              width: wp(25),
                              height: wp(25),
                              resizeMode: 'contain',
                              borderRadius: 10,
                            }}
                          />
                        </View>
                        <View style={{marginHorizontal: wp(4)}}>
                          <Text
                            numberOfLines={2}
                            style={{
                              color: '#1E1E1E',
                              fontSize: 14,
                              fontWeight: 'bold',
                              width: wp(60),
                            }}>
                            {item.nama_produk}
                          </Text>
                          <Text
                            numberOfLines={2}
                            style={{
                              width: wp(60),
                              lineHeight: 20,
                              marginTop: hp(1),
                            }}>
                            {item.deskripsi}
                          </Text>
                          <View
                            style={{
                              flexDirection: 'row',
                              marginTop: hp(0),
                              justifyContent: 'space-between',
                              alignItems: 'center',
                            }}>
                            <Text
                              style={{
                                color: '#1E1E1E',
                                fontSize: 16,
                                fontWeight: 'bold',
                              }}>
                              Rp {item.total}
                            </Text>
                            <View
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                              }}>
                              <TouchableOpacity
                                onPress={() => this.update_cart_decrement(item)}
                                style={{
                                  borderWidth: 0.8,
                                  borderColor: '#39C5A3',
                                  padding: wp(1),
                                  borderRadius: 5,
                                }}>
                                <Icon
                                  name="minus"
                                  size={20}
                                  color={'#39C5A3'}
                                />
                              </TouchableOpacity>
                              <Text
                                style={{
                                  color: '#1E1E1E',
                                  marginHorizontal: wp(4),
                                  fontSize: 20,
                                }}>
                                {item.qty}
                              </Text>
                              <TouchableOpacity
                                onPress={() => this.update_cart_increment(item)}
                                style={{
                                  backgroundColor: '#39C5A3',
                                  padding: wp(1),
                                  borderRadius: 5,
                                }}>
                                <Icon name="plus" size={20} color={'#fff'} />
                              </TouchableOpacity>
                              <TouchableOpacity
                                onPress={() => this.delete_cart(item)}
                                style={{
                                  // backgroundColor: '#39C5A3',
                                  // padding: wp(1),
                                  borderRadius: 5,
                                  marginLeft: wp(4),
                                }}>
                                <Icon name="delete" size={20} color={'red'} />
                              </TouchableOpacity>
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>
                    <View
                      style={{
                        borderBottomWidth: 0.5,
                        borderBottomColor: '#CFCFCF',
                      }}
                    />
                  </View>
                ))}
            </ScrollView>

            {/* ====================================================================================================== */}

            <View
              style={{
                marginHorizontal: wp(4),
                marginVertical: hp(2),
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <View style={{marginBottom: hp(1)}}>
                <Text style={{color: '#C9C9C9', marginBottom: 5}}>
                  Total Harga:
                </Text>
                <Text
                  style={{fontWeight: 'bold', color: '#1E1E1E', fontSize: 18}}>
                  Rp {totalprice.total}
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Checkout')}
                style={{
                  backgroundColor: '#39C5A3',
                  paddingVertical: hp(1.5),
                  width: wp(40),
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 7,
                }}>
                <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
                  Beli ({totalcart.total_qty})
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        ) : (
          <NotLogin onPress={() => this.props.navigation.navigate('Login')} />
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default Cart;
