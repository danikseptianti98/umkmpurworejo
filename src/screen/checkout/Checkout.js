import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Modal,
} from 'react-native';
import React, {Component} from 'react';
import HeaderStack from '../../components/HeaderStack';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export class Checkout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id_user: null,
      cart: [],
      totalprice: 0,
      totalcart: 0,
      ongkir: 0,
      address: [],
      kurir: '',
      // url: 'http://192.168.1.92/restapi/',
      // url: 'http://192.168.0.108/restapi/',
      url: 'https://masterelectronic.000webhostapp.com/restapi/',


      visible: false,
    };
  }
  async componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.getCart();
      this.getTotal();
      this.getAddress();
    });
  }
  async componentWillUnmount() {
    this._unsubscribe();
  }
  getCart = async () => {
    const value = await AsyncStorage.getItem('id_user');
    const response = await axios.get(
      `${this.state.url}cart.php?function=get_list_cart&id_user=` + value,
    );
    // console.log(response.data);
    this.setState({cart: response.data.data});
  };

  getAddress = async () => {
    const value = await AsyncStorage.getItem('id_user');

    const response = await axios.get(
      `${this.state.url}address.php?function=get_select_address&id_user=` +
        value,
    );
    response.data.data.map(item => {
      // console.log('alamat',response.data.data);
      this.setState({address: item});
    });
    // console.log(response.data);
  };

  getTotal = async () => {
    const value = await AsyncStorage.getItem('id_user');
    const response = await axios.get(
      `${this.state.url}cart.php?function=get_total_price&id_user=` + value,
    );
    // console.log('TOTAL', response.data);
    response.data.data.map(item => {
      // console.log('item',item.total);
      this.setState({
        totalprice: item,
        totalcart: item,
      });
    });
  };
  render() {
    const {cart, totalprice, totalcart, ongkir, address, visible} = this.state;
    console.log('totalprice', totalprice);
    return (
      <View style={styles.container}>
        <HeaderStack
          title={'Pengiriman'}
          onPress={() => this.props.navigation.goBack()}
        />
        <ScrollView>
          <View
            style={{
              paddingHorizontal: wp(5),
              paddingVertical: hp(1),
              borderBottomColor: '#C9C9C9',
              borderBottomWidth: 0.4,
              borderTopColor: '#C9C9C9',
              borderTopWidth: 0.4,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text style={{color: '#292929', fontWeight: 'bold'}}>
              Alamat Pengiriman
            </Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('ListAddress')}
              style={{
                paddingVertical: hp(1),
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 7,
              }}>
              <Text
                style={{color: '#39C5A3', fontSize: 12, fontWeight: 'bold'}}>
                Pilih Alamat Lain
              </Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              paddingHorizontal: wp(5),
              paddingVertical: hp(2.5),
              borderBottomColor: 'rgba(141,150,170,0.4)',
              borderBottomWidth: 3,
              // borderTopColor: '#C9C9C9',
              // borderTopWidth: 0.4,
            }}>
            <Text style={{color: '#292929', fontWeight: 'bold', fontSize: 12}}>
              {address.nama_penerima}
            </Text>
            <Text style={{color: '#C9C9C9', fontSize: 12, marginTop: 5}}>
              {address.no_hp}
            </Text>
            <Text style={{color: '#C9C9C9', fontSize: 12, marginTop: 5}}>
              {address.alamat_lengkap} | {address.kode_pos}
            </Text>
          </View>

          <View style={{marginHorizontal: wp(5), marginVertical: hp(2)}}>
            <Text
              style={{
                color: '#292929',
                fontWeight: 'bold',
                marginBottom: hp(2),
              }}>
              Daftar Pembelian
            </Text>
            {cart.map(item => (
              <View
                style={{
                  flexDirection: 'row',
                  marginVertical: hp(1),
                  alignItems: 'center',
                }}>
                <View
                  style={
                    {
                      // backgroundColor: '#F1F1F1',
                      // padding: wp(4),
                      // marginVertical: hp(1),
                    }
                  }>
                  <Image
                    source={{
                      uri: `${this.state.url}image/${item.gambar}`,
                    }}
                    style={{
                      width: wp(20),
                      height: wp(20),
                      resizeMode: 'contain',
                      borderRadius: 10,
                    }}
                  />
                </View>
                <View style={{marginVertical: hp(0), marginHorizontal: wp(4)}}>
                  <Text
                    style={{
                      color: '#1E1E1E',
                      fontSize: 14,
                      fontWeight: 'bold',
                      width: wp(65),
                    }}>
                    {item.nama_produk}
                  </Text>

                  <Text
                    style={{
                      color: '#C9C9C9',
                      fontSize: 10,
                    }}>
                    {item.qty} Barang
                  </Text>

                  <View
                    style={{
                      flexDirection: 'row',
                      marginTop: hp(1),
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        color: '#1E1E1E',
                        fontSize: 14,
                        fontWeight: 'bold',
                      }}>
                      Rp {item.total}
                    </Text>
                  </View>
                </View>
              </View>
            ))}
          </View>

          <TouchableOpacity
            onPress={() => this.setState({visible: true})}
            style={{
              marginHorizontal: wp(5),
              marginVertical: hp(2),
              paddingHorizontal: wp(3),
              paddingVertical: hp(2),
              borderWidth: 0.5,
              borderColor: '#C9C9C9',
              borderRadius: 8,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon name="truck" color={'#39C5A3'} size={20} />
              {this.state.kurir === '' ? (
                <Text
                  style={{
                    color: '#292929',
                    fontWeight: 'bold',
                    marginLeft: wp(3),
                  }}>
                  Pilih Pengiriman
                </Text>
              ) : (
                <Text
                  style={{
                    color: '#292929',
                    fontWeight: 'bold',
                    marginLeft: wp(3),
                  }}>
                  {this.state.kurir}
                </Text>
              )}
            </View>
            <Icon name="chevron-right" color={'#292929'} size={20} />
          </TouchableOpacity>

          <View
            style={{
              borderTopColor: 'rgba(141,150,170,0.4)',
              borderTopWidth: 3,
            }}>
            <View style={{marginHorizontal: wp(5), marginVertical: hp(2)}}>
              <Text
                style={{
                  color: '#292929',
                  fontWeight: 'bold',
                  marginBottom: hp(2),
                }}>
                Ringkasan Belanja
              </Text>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginVertical: hp(1),
                }}>
                <Text>Total Harga</Text>
                <Text>Rp {totalprice.total}</Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginVertical: hp(1),
                }}>
                <Text>Total Ongkos Kirim</Text>
                <Text>Rp {ongkir}</Text>
              </View>
            </View>
          </View>
        </ScrollView>
        <View
          style={{
            marginHorizontal: wp(4),
            marginVertical: hp(2),
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View style={{marginBottom: hp(1)}}>
            <Text style={{color: '#292929', marginBottom: 5}}>
              Total Tagihan
            </Text>
            <Text style={{fontWeight: 'bold', color: '#1E1E1E', fontSize: 18}}>
              Rp {parseInt(totalprice.total ) + parseInt(ongkir)}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('Pembayaran', {
                amount: parseInt(totalprice.total ) + parseInt(ongkir),
                ongkir: ongkir,
                kurir: this.state.kurir,
                
              })
            }
            style={{
              backgroundColor: '#39C5A3',
              paddingVertical: hp(1.5),
              width: wp(40),
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 7,
            }}>
            <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
              Pilih Pembayaran
            </Text>
          </TouchableOpacity>
        </View>


        <Modal
          style={styles.centeredView}
          animationType="slide"
          transparent={true}
          visible={visible}
          // onRequestClose={() => {
          //   Alert.alert('Modal has been closed.');
          //   setModalVisible(!modalVisible);
          // }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Text
                  style={[
                    styles.modalText,
                    {color: '#1E1E1E', fontWeight: 'bold', fontSize: 18},
                  ]}>
                  Pilih Pengiriman
                </Text>
                <TouchableOpacity onPress={() => this.setState({visible: false})}>
                  <Icon name="close" size={25} color={'#1E1E1E'} />
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    kurir: 'J&T Express',
                    ongkir: 25000,
                    visible: false,
                  })
                }
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  marginVertical: hp(1),
                }}>
                <Image
                  source={{
                    uri: 'https://2.bp.blogspot.com/-VLat0qXitdo/W-gk_W0f-nI/AAAAAAAAE8c/847Tyk62QY8Y8oA-Xsx1B1ntXt0qWl0RgCLcBGAs/w1200-h630-p-k-no-nu/Logo%2BJ%2B%2526%2BT%2BVector%2BPNG%2BHD.png',
                  }}
                  style={{width: wp(30), height: hp(5), resizeMode: 'contain'}}
                />
                <Text style={{color: '#1E1E1E', fontSize: 16}}>Rp 25000</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    kurir: 'anteraja',
                    ongkir: 20000,
                    visible: false,
                  })
                }
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  marginVertical: hp(1),
                }}>
                <Image
                  source={{
                    uri: 'https://1.bp.blogspot.com/-_tPtINW4X6Y/YTTahY2lC7I/AAAAAAAAI5o/3-edRsMpdYQ3wYJSrKzKkJnWrhEejiiBgCLcBGAsYHQ/s1254/logo-anteraja.png',
                  }}
                  style={{width: wp(30), height: hp(5), resizeMode: 'contain'}}
                />
                <Text style={{color: '#1E1E1E', fontSize: 16}}>Rp 20000</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    // paddingBottom: hp(5),
  },
  centeredView: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'rgba(60, 60, 60, 0.7)',
    // marginTop: 22,
  },
  modalView: {
    // margin: 20,
    position: 'absolute',
    bottom: 0,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: wp(5),
    // alignItems: 'center',
    shadowColor: '#000',
    width: wp(100),
    height: hp(25),
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    // marginBottom: 15,
    textAlign: 'center',
  },
});

export default Checkout;
