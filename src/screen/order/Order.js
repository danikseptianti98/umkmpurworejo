import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React, {Component} from 'react';
import HeaderTop from '../../components/HeaderTop';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import NotLogin from '../../components/NotLogin';

export class Order extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id_user: null,
      order: [],
      idUser: null,
      // url: 'http://192.168.1.92/restapi/',
      // url: 'http://192.168.0.108/restapi/',
      url: 'https://masterelectronic.000webhostapp.com/restapi/',


    };
  }
  async componentDidMount() {
    const value = await AsyncStorage.getItem('id_user');
    if (value !== null) {
      this.setState({idUser: value});
    }
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.getOrder();
    });
  }
  async componentWillUnmount() {
    this._unsubscribe();
  }
 
  getOrder = async () => {
    const value = await AsyncStorage.getItem('id_user');
    const response = await axios.get(
      `${this.state.url}transaksi.php?function=get_list_transaksi&id_user=` +
        value,
    );
    // console.log(response.data);
    this.setState({order: response.data.data});
  };

  async getUser() {
    const value = await AsyncStorage.getItem('id_user');
    if (value !== null) {
      this.setState({idUser: value});
    }
  }

  render() {
    const {order, idUser} = this.state;
    console.log('idUser', idUser);
    return (
      <View style={styles.container}>
        <HeaderTop title={'Riwayat Transaksi'} />
        {idUser !== null ? (
          <ScrollView>
            {order !== undefined &&
              order.map(item => (
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate('DetailOrder', {info: item})
                  }
                  style={{
                    marginHorizontal: wp(4),
                    paddingHorizontal: wp(3),
                    elevation: 10,
                    borderRadius: 10,
                    paddingVertical: hp(2),
                    // borderWidth: 0.4,
                    // borderColor: '#39C5A3',
                    marginVertical: hp(1),
                    backgroundColor: '#fff',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      borderBottomColor: '#C9C9C9',
                      borderBottomWidth: 0.3,
                      paddingBottom: hp(2),
                    }}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Icon name="shopping" size={20} color={'#39C5A3'} />
                      <View style={{marginHorizontal: wp(3)}}>
                        <Text style={{color: '#292929', fontWeight: 'bold'}}>
                      ORDER ID {item.ido}
                        </Text>
                        <Text style={{fontSize: 12, color: '#C9C9C9'}}>
                          {moment(item.tgl_bayar).format('LL')}
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        backgroundColor: '#39C5A3',
                        paddingVertical: wp(1),
                        paddingHorizontal: wp(3),
                        borderRadius: 5,
                      }}>
                      <Text style={{color: '#fff', fontSize: 10}}>
                        {item.status_transaksi}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      marginVertical: hp(2),
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={{
                        uri: `${this.state.url}image/${item.gambar}`,
                      }}
                      style={{
                        width: wp(10),
                        height: wp(10),
                        borderRadius: 8,
                        marginRight: wp(2),
                      }}
                    />
                    <View>
                      <Text
                        numberOfLines={1}
                        style={{
                          color: '#292929',
                          fontWeight: 'bold',
                          width: wp(70),
                        }}>
                        {item.nama_produk}
                      </Text>
                      <Text
                        style={{
                          color: '#C9C9C9',
                          fontSize: 12,
                          marginTop: hp(0.3),
                        }}>
                        {item.qty} Barang
                      </Text>
                    </View>
                  </View>
                  <View>
                    <Text style={{color: '#292929', fontSize: 12}}>
                      Total Belanja
                    </Text>
                    <Text
                      style={{
                        color: '#292929',
                        fontSize: 14,
                        fontWeight: 'bold',
                      }}>
                      Rp {item.total_bayar}
                    </Text>
                  </View>
                </TouchableOpacity>
              ))}
          </ScrollView>
        ) : (
          <NotLogin onPress={() => this.props.navigation.navigate('Login')} />
        )}

        {/* <Text>Order</Text> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    // paddingBottom: hp(5),
  },
});
export default Order;
