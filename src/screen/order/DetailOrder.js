import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React, {Component} from 'react';
import HeaderStack from '../../components/HeaderStack';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import base64 from 'base-64';

const serverKey = 'SB-Mid-server-sesemWaZXRUHNxY0RKuzG3ml';
const base64Key = base64.encode(serverKey);

export class DetailOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id_user: null,
      detail: [],
      pembayaran: [],
      pengiriman: [],
      // url: 'http://192.168.1.92/restapi/',
      va_number: '',
      // url: 'http://192.168.0.108/restapi/',
      url: 'https://masterelectronic.000webhostapp.com/restapi/',


    };
  }

  async componentDidMount() {
    // this._unsubscribe = this.props.navigation.addListener('focus', () => {
    this.getDetailInfo();
    this.checkPayment();
    this.getPengirirman();
    // });
  }
  // async componentWillUnmount() {
  //   this._unsubscribe();
  // }

  async getDetailInfo() {
    const {info} = this.props.route.params;
    console.log('id order_id', info);
    const response = await axios.get(
      `${this.state.url}transaksi.php?function=get_detail_transaksi&order_id=` +
        info.order_id,
    );
    // console.log(response.data);
    this.setState({detail: response.data.data});
  }

  async getPengirirman() {
    const {info} = this.props.route.params;
    // console.log('id transaksi', info.id_transaksi);
    const response = await axios.get(
      `${this.state.url}transaksi.php?function=get_pengiriman&order_id=` +
        info.ido,
    );
    console.log('pengiriman', response.data.data);
    // if (response.data.data !== undefined) {
     
    // }
    response.data.data.map(item => {
      this.setState({pengiriman: item});
    });

    // console.log(response.data);
  }

  async getstatus() {
    const {info} = this.props.route.params;
    // url for get the status of the transactions
    // this url is for sandbox
    const url = `https://api.sandbox.midtrans.com/v2/${info.order_id}/status`;

    // fetch data
    const response = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Basic ' + base64Key,
      },
    });
    return response.json();
  }

  checkPayment() {
    this.setState({loading: true});
    // setLoading(true);
    this.getstatus().then(data => {
      if (data.status_code == 200) {
        // console.log(data);
        this.setState({loading: false});
        // data.map((item) => {
        this.setState({pembayaran: data});
        this.setState({va_number: data.va_numbers[0].va_number});
        // })

        // setLoading(false);
        // alert('This Order ID has been paid');
      } else {
        console.log(data);
        this.setState({loading: false});
        this.setState({pembayaran: data});
        this.setState({va_number: data.va_numbers[0].va_number});

        // setLoading(false);
        // alert('This Order ID has not been paid');
      }
    });
  }

  render() {
    const {info} = this.props.route.params;
    const {detail, pembayaran, pengiriman, va_number} = this.state;
    // console.log('pengiriman', pengiriman);
    // const {info} = this.props.route.params;
    // console.log('id transaksi', info.id_transaksi);

    return (
      <View style={styles.container}>
        <HeaderStack
          title={'Detail Pesanan'}
          onPress={() => this.props.navigation.goBack()}
        />
        <ScrollView>
          <View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginHorizontal: wp(5),
                borderBottomColor: 'rgba(228, 232, 251, 0.9)',
                borderBottomWidth: 0.5,
                paddingVertical: hp(2),
              }}>
              <Text style={{color: '#292929', fontWeight: 'bold'}}>
                Status Pesanan
              </Text>
              <Text style={{color: '#39C5A3', fontWeight: 'bold'}}>
                {info.status_transaksi}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginHorizontal: wp(5),
                borderBottomColor: 'rgba(228, 232, 251, 0.5)',
                borderBottomWidth: 5,
                paddingVertical: hp(2),
              }}>
              <Text style={{color: '#C9C9C9'}}>Tanggal Pembelian</Text>
              <Text style={{color: '#292929'}}>
                {moment(info.tgl_bayar).format('LL')}
              </Text>
            </View>
          </View>

          <View
            style={{
              marginHorizontal: wp(5),
              paddingVertical: hp(2),
            }}>
            <Text style={{color: '#292929', fontWeight: 'bold', fontSize: 16}}>
              Detail Produk
            </Text>
          </View>
          <View
            style={{
              paddingBottom: hp(2),

              borderBottomColor: 'rgba(228, 232, 251, 0.5)',
              borderBottomWidth: 5,
            }}>
            {detail !== undefined &&
              detail.map(item => (
                <TouchableOpacity
                  // onPress={() => this.props.navigation.navigate('DetailOrder')}
                  style={{
                    marginHorizontal: wp(4),
                    paddingHorizontal: wp(3),
                    elevation: 10,
                    borderRadius: 10,
                    paddingVertical: hp(2),
                    // borderWidth: 0.4,
                    // borderColor: '#39C5A3',
                    marginVertical: hp(1),
                    backgroundColor: '#fff',
                  }}>
                  <View
                    style={{
                      marginVertical: hp(2),
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={{
                        uri: `${this.state.url}image/${item.gambar}`,
                      }}
                      style={{
                        width: wp(10),
                        height: wp(10),
                        borderRadius: 8,
                        marginRight: wp(2),
                      }}
                    />
                    <View>
                      <Text
                        numberOfLines={1}
                        style={{
                          color: '#292929',
                          fontWeight: 'bold',
                          width: wp(70),
                        }}>
                        {item.nama_produk}
                      </Text>
                      <Text
                        style={{
                          color: '#C9C9C9',
                          fontSize: 12,
                          marginTop: hp(0.3),
                        }}>
                        {item.qty} Barang
                      </Text>
                    </View>
                  </View>
                  <View>
                    <Text style={{color: '#292929', fontSize: 12}}>
                      Total Harga
                    </Text>
                    <Text
                      style={{
                        color: '#292929',
                        fontSize: 14,
                        fontWeight: 'bold',
                      }}>
                      Rp {item.total}
                    </Text>
                  </View>
                </TouchableOpacity>
              ))}
          </View>
          <View
            style={{
              borderBottomColor: 'rgba(228, 232, 251, 0.5)',
              borderBottomWidth: 5,
            }}>
            <View
              style={{
                marginHorizontal: wp(5),
                paddingVertical: hp(2),
              }}>
              <Text
                style={{color: '#292929', fontWeight: 'bold', fontSize: 16}}>
                Info Pengiriman
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  marginVertical: hp(1),
                  marginTop: hp(3),
                }}>
                <Text style={{width: wp(20)}}>Kurir</Text>
                <Text style={{color: '#292929'}}>{pengiriman.kurir} </Text>
              </View>
              <View style={{flexDirection: 'row', marginVertical: hp(1)}}>
                <Text style={{width: wp(20)}}>No Resi</Text>
                <Text style={{color: '#292929'}}>
                  {pengiriman.resi === undefined
                    ? pengiriman.resi
                    : 'Resi belum diinputkan'}{' '}
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginVertical: hp(1)}}>
                <Text style={{width: wp(20)}}>Alamat</Text>
                <View>
                  <Text style={{color: '#292929', fontWeight: 'bold'}}>
                    {pengiriman.nama_penerima}
                  </Text>
                  <Text style={{color: '#292929'}}> {pengiriman.no_hp} </Text>
                  <Text
                    style={{
                      color: '#292929',
                      width: wp(70),
                      marginTop: hp(0.3),
                    }}>
                    {pengiriman.alamat_lengkap} | {pengiriman.kode_pos}
                  </Text>
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              borderBottomColor: 'rgba(228, 232, 251, 0.5)',
              borderBottomWidth: 0,
            }}>
            <View
              style={{
                marginHorizontal: wp(5),
                paddingVertical: hp(2),
              }}>
              <Text
                style={{color: '#292929', fontWeight: 'bold', fontSize: 16}}>
                Rincian Pembayaran
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  marginVertical: hp(1),
                  marginTop: hp(3),
                  justifyContent: 'space-between',
                }}>
                <Text style={{width: wp(35), color: '#292929'}}>
                  Metode Pembayaran
                </Text>
                <Text style={{color: '#292929'}}>
                  {pembayaran.payment_type}
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  marginVertical: hp(1),
                  justifyContent: 'space-between',
                }}>
                <Text style={{width: wp(35), color: '#292929'}}>
                  Status Pembayaran
                </Text>
                <Text style={{color: '#292929'}}>
                  {pembayaran.transaction_status}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  marginVertical: hp(1),
                  justifyContent: 'space-between',
                }}>
                <Text style={{width: wp(35), color: '#292929'}}>
                  No Virtual Account
                </Text>
                <Text style={{color: '#292929'}}>
                  {va_number !== undefined && va_number}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  marginVertical: hp(1),
                  justifyContent: 'space-between',
                }}>
                <Text style={{width: wp(35), color: '#292929'}}>
                  Total Harga
                </Text>
                <Text style={{color: '#292929'}}>Rp {info.total_bayar}</Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  marginVertical: hp(1),
                  justifyContent: 'space-between',
                }}>
                <Text style={{width: wp(35), color: '#292929'}}>
                  Total Ongkos Kirim
                </Text>
                <Text style={{color: '#292929'}}>Rp {info.ongkir}</Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  marginVertical: hp(1),
                  justifyContent: 'space-between',
                }}>
                <Text
                  style={{width: wp(35), color: '#292929', fontWeight: 'bold'}}>
                  Total Belanja
                </Text>
                <Text style={{color: '#292929', fontWeight: 'bold'}}>
                  Rp {parseInt(info.ongkir) + parseInt(info.total_bayar)}
                </Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    // paddingBottom: hp(5),
  },
});

export default DetailOrder;
