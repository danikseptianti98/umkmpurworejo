import {Text, StyleSheet, View, Image, TouchableOpacity} from 'react-native';
import React, {Component} from 'react';
import HeaderTop from '../../components/HeaderTop';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import NotLogin from '../../components/NotLogin';
import axios from 'axios';

export class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      idUser: null,
      user: [],
      // url: 'http://192.168.1.92/restapi/',
      // url: 'http://192.168.0.108/restapi/',
      url: 'https://masterelectronic.000webhostapp.com/restapi/',

    
    };
  }

  async componentDidMount() {
    const value = await AsyncStorage.getItem('id_user');
    this.setState({idUser: value});
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.getUser();
    });
  }

  async componentWillUnmount() {
    this._unsubscribe();
  }

  getUser = async () => {
    const value = await AsyncStorage.getItem('id_user');
    const response = await axios.get(
      `${this.state.url}user.php?function=getUser&id_user=`+value,
    );
    console.log('DATA USER', response.data);
    response.data.data.map(item => {
      this.setState({user: item});
    });
  };

  async logout() {
    // await AsyncStorage.clear();
    // this.setState({user: []});
    await AsyncStorage.removeItem('id_user');
    this.props.navigation.navigate('Login');
  }
  render() {
    const {idUser, user} = this.state;

    return (
      <View style={{flex: 1}}>
        <View style={{borderBottomWidth: 1, borderBottomColor: '#E4E8FB'}}>
          <HeaderTop title={'Account'} />
        </View>
        {idUser !== null ? (
          <View>
            <View
              style={{
                backgroundColor: '#fff',
                flexDirection: 'row',
                paddingHorizontal: wp(4),
                paddingVertical: hp(2),
                alignItems: 'center',
              }}>
              <Image
                source={{
                  uri: 'https://cdn.icon-icons.com/icons2/1161/PNG/512/1487716857-user_81635.png',
                }}
                style={{width: wp(15), height: wp(15), marginRight: wp(3)}}
              />
              <View>
                <Text style={{color: '#C9C9C9'}}>{user.email}</Text>
                <Text
                  style={{
                    color: '#1E1E1E',
                    fontWeight: 'bold',
                    fontSize: 18,
                    marginTop: hp(0.3),
                  }}>
                  {user.nama_user}
                </Text>
              </View>
            </View>
            <Text
              style={{
                marginVertical: hp(2),
                marginHorizontal: wp(4),
                fontSize: 16,
                color: '#1E1E1E',
                fontWeight: 'bold',
              }}>
              Account
            </Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('ListAddress')}
              style={{
                backgroundColor: '#fff',
                marginVertical: hp(1),
                marginHorizontal: wp(4),
                paddingHorizontal: wp(5),
                paddingVertical: hp(2.3),
                borderRadius: 9,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: '#1E1E1E',
                }}>
                Daftar Alamat
              </Text>
              <Icon name="chevron-right" size={25} color={'#1E1E1E'} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.logout()}
              style={{
                backgroundColor: '#fff',
                marginVertical: hp(1),
                marginHorizontal: wp(4),
                paddingHorizontal: wp(5),
                paddingVertical: hp(2.3),
                borderRadius: 9,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: 'red',
                }}>
                Keluar
              </Text>
              <Icon name="logout" size={25} color={'red'} />
            </TouchableOpacity>
          </View>
        ) : (
          <View style={{flex: 1, backgroundColor: '#fff'}}>
            <NotLogin onPress={() => this.props.navigation.navigate('Login')} />
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({});
export default Account;
