import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  StyleSheet,
} from 'react-native';
import {WebView} from 'react-native-webview';
import base64 from 'base-64';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const serverKey = 'SB-Mid-server-sesemWaZXRUHNxY0RKuzG3ml';
const base64Key = base64.encode(serverKey);
// order ID
const orderID = Math.floor(Math.random() * 10000000);

export class Pembayaran extends Component {
  constructor(props) {
    super(props);
    this.state = {
      detail: '',
      loading: false,
      transactions: false,
      // url: 'http://192.168.1.92/restapi/',
      // url: 'http://192.168.0.108/restapi/',
      url: 'https://masterelectronic.000webhostapp.com/restapi/',


    };
  }

  // on production dont place the server key here
  // dont forget add ":" in the end of the string

  componentDidMount() {
    this.midtrans().then(data => {
      // get the response and save it to the state
      console.log('response midtrans', data);
      this.setState({transactions: data});
    });
  }

  async addDetailTransaction() {
    const value = await AsyncStorage.getItem('id_user');

    let response = await axios.post(
      `${this.state.url}checkout.php?function=checkoutDetail`,
      {
        id_user: value,
        order_id: orderID,
        //other data key value pairs
      },
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
    console.log('response', response.data.data);
    if (response.data.data.status === 1) {
      // console.log('Add Transaksi', response.data);
    
    }
  }

  async addPengiriman() {
    const value = await AsyncStorage.getItem('id_user');
    let response = await axios.post(
      `${this.state.url}checkout.php?function=addPengiriman`,
      {
        order_id: orderID,
        id_user: value,
        kurir: this.props.route.params.kurir,
        //other data key value pairs
      },
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
    console.log('response', response);
    if (response.data.status === 1) {
      console.log('Add Transaksi', response.data);
      // this.deleteCart()
      // this.setState({status: 200});
      // await AsyncStorage.setItem('id_user', response.data.id_user);
      // this.props.navigation.navigate('Home');
    }
  }

  async deleteCart() {
    const value = await AsyncStorage.getItem('id_user');
    let response = await axios.delete(
      `${this.state.url}transaksi.php?function=delete_cart&id_user=` + value,
      {
        //other data key value pairs
      },
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
    console.log('response delete', response.data);
  }
  async midtrans(user) {
    // url for transactions
    // this url is for sandbox
    const url = 'https://app.sandbox.midtrans.com/snap/v1/transactions';
    // use this url for production : https://app.midtrans.com

    // transactions data
    const data = {
      transaction_details: {
        order_id: orderID,
        gross_amount: this.props.route.params.amount,
      },
      // item_details: [
      //   {
      //     id: 'PRODUCTID1',
      //     price: 20000,
      //     quantity: 1,
      //     name: 'Product 1',
      //     category:x 'Clothes',
      //     merchant_name: 'Merchant',
      //   },
      //   {
      //     id: 'PRODUCTID2',
      //     price: 40000,
      //     quantity: 1,
      //     name: 'Product 2',
      //     category: 'Clothes',
      //     merchant_name: 'Merchant',
      //   },
      // ],
      credit_card: {
        secure: true,
      },
      customer_details: {
        first_name: 'budi',
        last_name: 'pratama',
        email: 'budi.pra@example.com',
        phone: '08111222333',
      },
    };

    // fetch data
    const responsePOST = await fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Basic ' + base64Key,
      },
      body: JSON.stringify(data),
    });

    const value = await AsyncStorage.getItem('id_user');

    let response = await axios.post(
      `${this.state.url}checkout.php?function=checkout`,
      {
        id_user: value,
        total_bayar: this.props.route.params.amount,
        order_id: orderID,
        ongkir: this.props.route.params.ongkir,
        tgl_bayar: '2022-12-06',
        status_transaksi: 'Menunggu Pembayaran',
        //other data key value pairs
      },
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
    console.log('response', response);
    if (response.data.status === 1) {
      console.log('Add Transaksi', response.data);
      this.addDetailTransaction();
      this.addPengiriman()
    }

    return responsePOST.json();
  }

  render() {
    const {transactions, loading} = this.state;
    console.log('transactions', transactions);
    return (
      <View style={styles.container}>
        {!transactions ? (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ActivityIndicator color="#fff" size="large" />
          </View>
        ) : (
          <>
            <WebView
              source={{
                uri: transactions.redirect_url,
              }}
              style={{flex: 1}}
            />

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('Transaction');
              }}
              style={{
                backgroundColor: '#3366FF',
                padding: 20,
                paddingVertical: 15,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
              disabled={loading}>
              <Text
                style={{
                  color: 'white',
                  fontWeight: 'bold',
                  fontSize: 18,
                }}>
                Lihat Transaksi
              </Text>
              {loading ? (
                <ActivityIndicator color="#fff" />
              ) : (
                <View />
                // <FontAwesome name="check-square" size={20} color="white" />
              )}
            </TouchableOpacity>
          </>
        )}
      </View>
      // <WebView
      //   source={{
      //     uri: transactions.redirect_url,
      //   }}
      //   style={{flex: 1}}
      // />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#f7f7f7',
  },
});

export default Pembayaran;
