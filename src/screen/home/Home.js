import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
} from 'react-native';
import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import HeaderHome from '../../components/HeaderHome';

const DATA = [
  {
    id: 1,
    title: 'Headset',
    image:
      'https://www.pngall.com/wp-content/uploads/5/Gaming-Headset-PNG-Image.png',
    desc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    price: 1200,
    bg: '#E4F2FB',
  },
  {
    id: 2,
    title: 'Smartphone',
    image: 'https://pngimg.com/uploads/iphone_11/iphone_11_PNG14.png',
    desc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    price: 1200,
    bg: '#FCD7FF',
  },
  {
    id: 3,
    title: 'Mackbook',
    image:
      'https://purepng.com/public/uploads/large/purepng.com-macbookmacbooknotebookcomputersapple-inmacbook-familyapple-laptops-17015283602492hsw5.png',
    desc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    price: 1200,
    bg: '#FDE1E6',
  },
  {
    id: 4,
    title: 'Speaker',
    image:
      'https://static.wixstatic.com/media/e81d35_5c8212df58fa487287506d51ebb8c27d~mv2.png/v1/fill/w_3000,h_3105,al_c/e81d35_5c8212df58fa487287506d51ebb8c27d~mv2.png',
    desc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    price: 1200,
    bg: '#E1FDE5',
  },
];

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      // url: 'http://192.168.1.92/restapi/',
      // url: 'http://192.168.0.108/restapi/',
      url: 'https://masterelectronic.000webhostapp.com/restapi/',

    };
  }

  componentDidMount() {
    this.getProducts();
  }
  getProducts = async () => {
    const response = await axios.get(
      // 'http://192.168.1.92/restapi/restapi.php?function=get_produk',
      `${this.state.url}restapi.php?function=get_produk`,
    );
    // console.log(response.data);
    this.setState({products: response.data.data});
  };
  add_cart = async item => {
    const value = await AsyncStorage.getItem('id_user');

    let response = await axios.post(
      `${this.state.url}cart.php?function=add_to_cart`,
      {
        id_user: value,
        id_produk: item.id_produk,
        qty: 1,
        total: item.harga,
        //other data key value pairs
      },
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
    console.log('response', response);
    if (response.data.status === 1) {
      // console.log(response.data);
      // this.setState({status: 200});
      // await AsyncStorage.setItem('id_user', response.data.id_user)
      this.props.navigation.navigate('Cart', {getCart: () => this.getCart()});
    }
  };
  render() {
    const {products} = this.state;
    return (
      <View style={styles.container}>
        <HeaderHome
          title={'Home'}
          onPress={() => this.props.navigation.navigate('Cart')}
        />
        <ScrollView>
          {/* <TouchableOpacity style={styles.search}>
          <Text style={{color: '#A1A9B4', fontSize: 16}}>
            Search for a product, cloth, etc...
          </Text>
          <Icon name="magnify" color={'#A1A9B4'} size={25} />
        </TouchableOpacity> */}
          <View
            style={{
              // borderBottomColor: '#F3F5F5',
              // borderBottomWidth: 2,
              paddingTop: hp(2),
            }}>
            {/* <ScrollView
            showsHorizontalScrollIndicator={false}
            horizontal
            style={{marginHorizontal: wp(4), marginVertical: wp(4)}}>
            <View>
              <TouchableOpacity style={styles.badge}>
                <Icon
                  name="tune-variant"
                  color={'#292929'}
                  size={25}
                  style={{margin: wp(1.1)}}
                />
              </TouchableOpacity>
            </View>
            {DATA.map(item => (
              <View>
                <TouchableOpacity style={styles.badge}>
                  <View style={styles.iamgebackground}>
                    <Image
                      source={{
                        uri: item.image,
                      }}
                      style={{
                        width: wp(7),
                        height: wp(7),
                        resizeMode: 'contain',
                      }}
                    />
                  </View>
                  <Text
                    style={{
                      color: '#292929',
                      fontSize: 14,
                      fontWeight: 'bold',
                      marginLeft: wp(1),
                    }}>
                    {item.title}
                  </Text>
                </TouchableOpacity>
              </View>
            ))}
          </ScrollView> */}
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: wp(4),
              marginVertical: hp(2),
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 18, color: '#292929', fontWeight: 'bold'}}>
              Semua Produk
            </Text>
            <Text
              style={{
                fontSize: 14,
                color: '#A1A9B4',
                fontWeight: 'bold',
              }}></Text>
          </View>
          <FlatList
            data={products}
            numColumns={2}
            columnWrapperStyle={styles.row}
            renderItem={({item}) => (
              <View style={{marginHorizontal: wp(3), marginVertical: hp(1)}}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('DetailProduk', {
                      detail: item,
                    });
                  }}>
                  <View
                    style={{
                      // padding: wp(5),
                      backgroundColor: '#fff',
                      borderRadius: 8,
                      // elevation: 25,
                    }}>
                    <Image
                      source={{
                        uri: `${this.state.url}/image/${item.gambar}`,
                      }}
                      style={{
                        borderRadius: 8,
                        width: wp(42),
                        height: wp(42),
                        // resizeMode: 'contain',
                        alignSelf: 'center',
                      }}
                    />
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginVertical: wp(2),
                      alignItems: 'center',
                    }}>
                    <Text
                      numberOfLines={2}
                      style={{
                        color: '#292929',
                        fontWeight: 'bold',
                        width: wp(30),
                      }}>
                      {item.nama_produk}
                    </Text>
                    {/* <TouchableOpacity
                      style={{
                        borderRadius: 30,
                        padding: wp(1),
                        borderWidth: 0.3,
                        borderColor: '#C9C9C9',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Icon name="heart" size={20} color={'#C9C9C9'} />
                    </TouchableOpacity> */}
                  </View>
                  <Text
                    numberOfLines={2}
                    style={{width: wp(42), lineHeight: 20, color: '#B0B0B0'}}>
                    {item.deskripsi}
                  </Text>
                  <View
                    style={{
                      marginTop: hp(1),
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        color: '#292929',
                        fontWeight: 'bold',
                        fontSize: 18,
                      }}>
                      Rp {item.harga}
                    </Text>
                    <TouchableOpacity onPress={() => this.add_cart(item)}>
                      <Icon name="plus-box" color={'#39C5A3'} size={30} />
                    </TouchableOpacity>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          />
          {/* <View>
          <ScrollView showsHorizontalScrollIndicator={false} horizontal>
            {products.map(item => (
              <View style={{marginHorizontal: wp(2)}}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('DetailProduk', {
                      detail: item
                    });
                  }}>
                  <View
                    style={{
                      padding: wp(5),
                      backgroundColor: '#fff',
                      borderRadius: 8,
                      elevation: 25,
                    }}>
                    <Image
                      source={{
                        uri: `http://192.168.1.92/restapi/image/${item.gambar}`
                      }}
                      style={{
                        width: wp(30),
                        height: wp(30),
                        resizeMode: 'contain',
                        alignSelf: 'center',
                      }}
                    />
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginVertical: wp(2),
                      alignItems: 'center',
                    }}>
                    <Text
                      numberOfLines={2}
                      style={{
                        color: '#292929',
                        fontWeight: 'bold',
                        width: wp(30),
                      }}>
                      {item.nama_produk}
                    </Text>
                    <TouchableOpacity
                      style={{
                        borderRadius: 30,
                        padding: wp(1),
                        borderWidth: 0.3,
                        borderColor: '#C9C9C9',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Icon name="heart" size={20} color={'#C9C9C9'} />
                    </TouchableOpacity>
                  </View>
                  <Text
                    numberOfLines={2}
                    style={{width: wp(42), lineHeight: 20, color: '#B0B0B0'}}>
                    {item.deskripsi}
                  </Text>
                  <View
                    style={{
                      marginTop: hp(1),
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        color: '#292929',
                        fontWeight: 'bold',
                        fontSize: 18,
                      }}>
                      Rp {item.harga}
                    </Text>
                    <TouchableOpacity onPress={() => this.add_cart(item)}>
                      <Icon name="plus-box" color={'#39C5A3'} size={30} />
                    </TouchableOpacity>
                  </View>
                </TouchableOpacity>
              </View>
            ))}
          </ScrollView>
        </View> */}
          <Image
            source={{
              uri: 'https://uidesign.gbtcdn.com/GB/image/2019/20190929_13044/banner.jpg?imbypass=true',
            }}
            style={{
              width: wp(95),
              height: hp(15),
              marginHorizontal: wp(4),
              resizeMode: 'contain',
              borderRadius: 10,
              alignSelf: 'center',
              marginVertical: hp(2),
            }}
          />
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingBottom: hp(5),
  },
  search: {
    backgroundColor: '#F8FAF9',
    marginHorizontal: wp(4),
    marginTop: hp(3),
    paddingHorizontal: wp(5),
    paddingVertical: hp(1.7),
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  badge: {
    borderWidth: 0.2,
    borderColor: '#A1A9B4',
    paddingHorizontal: wp(1.5),
    paddingVertical: wp(1.5),
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: wp(2),
  },
  iamgebackground: {
    backgroundColor: '#F6F6F6',
    padding: wp(1),
    borderRadius: 4,
  },
  row: {
    flex: 1,
    justifyContent: 'space-around',
  },
});

export default Home;
