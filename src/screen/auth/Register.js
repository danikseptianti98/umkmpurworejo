import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React, {Component} from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import RegisterImage from '../../assets/register.png';
import axios from 'axios';


export class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      fullname: '',
      loading: '',
      // url: 'http://192.168.1.92/restapi/',
      // url: 'http://192.168.0.108/restapi/',
      url: 'https://masterelectronic.000webhostapp.com/restapi/',



    };
  }

  async onRegister() {
    let response = await axios.post(
      `${this.state.url}auth.php?function=register`,
      {
        nama_user: this.state.fullname,
        email: this.state.email,
        password: this.state.password,
        //other data key value pairs
      },
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );

    if (response.data.status === 1) {
      this.props.navigation.navigate('Login', {
        status: 400,
        message: 'Berhasil Register. Masuk Dengan Email dan Password Anda.',
      });
    }

    // console.log('HALLO', response);
  }
  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={{padding: wp(5), marginTop: hp(1)}}>
          <Image
            source={RegisterImage}
            style={{width: wp(100), height: hp(32), resizeMode: 'contain'}}
          />
        </View>
        <View style={{paddingHorizontal: wp(7)}}>
          <Text style={{fontSize: 30, fontWeight: 'bold', color: '#1E1E1E'}}>
            Register
          </Text>
          <Text
            style={{color: '#C9C9C9', marginVertical: hp(1), lineHeight: 20}}>
            Daftar akun gratis dan nikmati kemudahan berbelanja di Aplikasi
            kami!
          </Text>
          <View
            style={{
              marginVertical: hp(2),
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon name="account-circle-outline" size={20} color="#C9C9C9" />
            <TextInput
              onChangeText={text => this.setState({fullname: text})}
              placeholder="Nama Lengkap"
              style={{
                borderBottomWidth: 0.5,
                borderBottomColor: '#C9C9C9',
                width: wp(80),
                paddingBottom: 0,
                marginLeft: wp(2),
              }}
            />
          </View>
          <View
            style={{
              marginVertical: hp(2),
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon name="at" size={20} color="#C9C9C9" />
            <TextInput
              onChangeText={text => this.setState({email: text})}
              placeholder="Email ID"
              style={{
                borderBottomWidth: 0.5,
                borderBottomColor: '#C9C9C9',
                width: wp(80),
                paddingBottom: 0,
                marginLeft: wp(2),
              }}
            />
          </View>
          <View
            style={{
              marginVertical: hp(2),
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon name="lock-outline" size={20} color="#C9C9C9" />
            <TextInput
              onChangeText={text => this.setState({password: text})}
              secureTextEntry={true}
              placeholder="Password"
              style={{
                borderBottomWidth: 0.5,
                borderBottomColor: '#C9C9C9',
                width: wp(80),
                paddingBottom: 0,
                marginLeft: wp(2),
              }}
            />
          </View>
          <TouchableOpacity
            onPress={() => this.onRegister()}
            style={{
              backgroundColor: '#39C5A3',
              paddingVertical: hp(2),
              width: wp(86),
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 7,
              marginVertical: hp(2),
            }}>
            <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
              Register
            </Text>
          </TouchableOpacity>
          {/* <Text style={{fontSize: 14, color: '#C9C9C9', textAlign: 'center'}}>
            atau
          </Text>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Pembayaran')}
            style={{
              backgroundColor: 'rgba(57, 197, 63, 0.1)',
              paddingVertical: hp(2),
              width: wp(86),
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 7,
              marginVertical: hp(2),
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon name="google" color={'#1E1E1E'} size={17} />
            <Text
              style={{
                color: '#1E1E1E',
                fontSize: 16,
                fontWeight: 'bold',
                marginLeft: wp(5),
              }}>
              Register dengan google
            </Text>
          </TouchableOpacity> */}
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: '#C9C9C9'}}>Sudah punya akun? </Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Login')}>
              <Text style={{color: '#39C5A3', fontWeight: 'bold'}}>Login</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    //   paddingBottom: hp(5),
  },
});

export default Register;
