import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React, {Component} from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import LoginImage from '../../assets/login.png';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      loading: '',
      status: null,
      // url: 'http://192.168.1.92/restapi/',
      url: 'https://masterelectronic.000webhostapp.com/restapi/',

      // url: 'http://192.168.0.108/restapi/',

    };
  }

  async onLogin() {
    let response = await axios.post(
      `${this.state.url}auth.php?function=login`,
      {
        email: this.state.email,
        password: this.state.password,
        //other data key value pairs
      },
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
    console.log('response', response);
    if (response.data.status === 1) {
      // console.log(response.data);
      this.setState({status: 200});
      await AsyncStorage.setItem('id_user', response.data.id_user);
      this.props.navigation.navigate('Home');
    }
  }
  render() {
    const {status} = this.state;
    return (
      <ScrollView style={styles.container}>
        <View style={{padding: wp(5), marginTop: hp(2)}}>
          <Image
            source={LoginImage}
            style={{width: wp(100), height: hp(35), resizeMode: 'contain'}}
          />
        </View>
        <View style={{paddingHorizontal: wp(7)}}>
          {status === 200 && <Text>Berhasil Login</Text>}

          <Text style={{fontSize: 30, fontWeight: 'bold', color: '#1E1E1E'}}>
            Login
          </Text>
          <Text
            style={{color: '#C9C9C9', marginVertical: hp(1), lineHeight: 20}}>
            Login untuk bisa berbelanja di aplikasi kami. Lebih mudah berbelan
            lewat di aplikasi dan nikmati kemudahannya!
          </Text>
          <View
            style={{
              marginVertical: hp(2),
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon name="at" size={20} color="#C9C9C9" />
            <TextInput
              onChangeText={text => this.setState({email: text})}
              placeholder="Email ID"
              style={{
                borderBottomWidth: 0.5,
                borderBottomColor: '#C9C9C9',
                width: wp(80),
                paddingBottom: 0,
                marginLeft: wp(2),
              }}
            />
          </View>
          <View
            style={{
              marginVertical: hp(2),
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon name="lock-outline" size={20} color="#C9C9C9" />
            <TextInput
              onChangeText={text => this.setState({password: text})}
              secureTextEntry={true}
              placeholder="Password"
              style={{
                borderBottomWidth: 0.5,
                borderBottomColor: '#C9C9C9',
                width: wp(80),
                paddingBottom: 0,
                marginLeft: wp(2),
              }}
            />
          </View>
          <TouchableOpacity
            onPress={() => this.onLogin()}
            style={{
              backgroundColor: '#39C5A3',
              paddingVertical: hp(2),
              width: wp(86),
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 7,
              marginVertical: hp(2),
            }}>
            <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
              Login
            </Text>
          </TouchableOpacity>
          {/* <Text style={{fontSize: 14, color: '#C9C9C9', textAlign: 'center'}}>
            atau
          </Text>
          <TouchableOpacity
            style={{
              backgroundColor: 'rgba(57, 197, 63, 0.1)',
              paddingVertical: hp(2),
              width: wp(86),
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 7,
              marginVertical: hp(2),
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon name="google" color={'#1E1E1E'} size={17} />
            <Text
              style={{
                color: '#1E1E1E',
                fontSize: 16,
                fontWeight: 'bold',
                marginLeft: wp(5),
              }}>
              Login dengan google
            </Text>
          </TouchableOpacity> */}
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: '#C9C9C9'}}>Belum punya akun? </Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Register')}>
              <Text style={{color: '#39C5A3', fontWeight: 'bold'}}>
                Register
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    //   paddingBottom: hp(5),
  },
});

export default Login;
