import {
  Text,
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import React, {Component} from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import HeaderStack from '../../components/HeaderStack';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

export class AddAddress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      penerima: '',
      nohp: null,
      alamat: '',
      kodepos: '',
      url: 'https://masterelectronic.000webhostapp.com/restapi/',

      // url: 'http://192.168.1.92/restapi/',
      // url: 'http://192.168.0.108/restapi/',

    };
  }

  async inputAddress() {
    const value = await AsyncStorage.getItem('id_user');
    let response = await axios.post(
      `${this.state.url}address.php?function=add_address`,
      {
        id_user: value,
        nama_penerima: this.state.penerima,
        no_hp: this.state.nohp,
        alamat_lengkap: this.state.alamat,
        kode_pos: this.state.kodepos,
        is_select: 'false',
        //other data key value pairs
      },
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
    console.log(response);
    if (response.data.status === 1) {
      this.props.navigation.navigate('ListAddress', {
        getAddress: () => this.getAddress(),
      });
    }
  }
  render() {
    return (
      <ScrollView style={styles.container}>
        <HeaderStack
          title={'Tambah Alamat'}
          onPress={() => this.props.navigation.goBack()}
        />
        <View style={{paddingHorizontal: wp(5)}}>
          <View
            style={{
              marginVertical: hp(2),
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon name="account-circle-outline" size={20} color="#C9C9C9" />
            <TextInput
              onChangeText={text => this.setState({penerima: text})}
              placeholder="Nama Penerima"
              style={{
                borderBottomWidth: 0.5,
                borderBottomColor: '#C9C9C9',
                width: wp(85),
                paddingBottom: 0,
                marginLeft: wp(2),
              }}
            />
          </View>
          <View
            style={{
              marginVertical: hp(2),
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon name="phone" size={20} color="#C9C9C9" />
            <TextInput
              onChangeText={text => this.setState({nohp: text})}
              placeholder="No Telepon"
              style={{
                borderBottomWidth: 0.5,
                borderBottomColor: '#C9C9C9',
                width: wp(85),
                paddingBottom: 0,
                marginLeft: wp(2),
              }}
            />
          </View>

          <View
            style={{
              marginVertical: hp(2),
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon name="lock-outline" size={20} color="#C9C9C9" />
            <TextInput
              onChangeText={text => this.setState({alamat: text})}
              placeholder="Alamat Lengkap"
              style={{
                borderBottomWidth: 0.5,
                borderBottomColor: '#C9C9C9',
                width: wp(85),
                paddingBottom: 0,
                marginLeft: wp(2),
              }}
            />
          </View>
          <View
            style={{
              marginVertical: hp(2),
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Icon name="lock-outline" size={20} color="#C9C9C9" />
            <TextInput
              onChangeText={text => this.setState({kodepos: text})}
              placeholder="Zip Code"
              style={{
                borderBottomWidth: 0.5,
                borderBottomColor: '#C9C9C9',
                width: wp(85),
                paddingBottom: 0,
                marginLeft: wp(2),
              }}
            />
          </View>
          <TouchableOpacity
            onPress={() => this.inputAddress()}
            style={{
              backgroundColor: '#39C5A3',
              paddingVertical: hp(2),
              width: wp(90),
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 7,
              marginVertical: hp(2),
            }}>
            <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
              Tambah Alamat
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    //   paddingBottom: hp(5),
  },
});

export default AddAddress;
