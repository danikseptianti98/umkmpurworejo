import {
  Text,
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import React, {Component} from 'react';
import HeaderStack from '../../components/HeaderStack';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export class ListAddress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: [],
      // url: 'http://192.168.1.92/restapi/',
      // url: 'http://192.168.0.108/restapi/',
      url: 'https://masterelectronic.000webhostapp.com/restapi/',

    };
  }

  componentDidMount() {
    this.getAddress();
  }
  // componentWillUnmount() {
  //   if (this.props.route.params.getAddress !== undefined) {
  //     this.props.route.params.getAddress;
  //   }
  // }
  getAddress = async () => {
    const value = await AsyncStorage.getItem('id_user');

    const response = await axios.get(
      `${this.state.url}address.php?function=get_list_address&id_user=` +
        value,
    );
    console.log(response.data);
    this.setState({address: response.data.data});
  };

  async setAddress(item) {
    const value = await AsyncStorage.getItem('id_user');

    let response = await axios.post(
      `${this.state.url}address.php?function=set_alamat`,
      {
        id_alamat: item.id_alamat,
        id_user: value,
        //other data key value pairs
      },
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
    console.log('response', response);
    if (response.data.status === 1) {
      this.getAddress();
      this.props.navigation.navigate('Checkout');
    }
  }

  render() {
    const {address} = this.state;

    return (
      <View style={styles.container}>
        <HeaderStack
          title={'Daftar Alamat'}
          onPress={() => this.props.navigation.goBack()}
        />
        <ScrollView style={{paddingHorizontal: wp(5)}}>
          {address!== undefined && address.map(item => (
            <TouchableOpacity
              onPress={() => this.setAddress(item)}
              style={{
                borderWidth: 0.8,
                borderColor: item.is_select === 'true' ? '#39C5A3' : '#C9C9C9',
                padding: wp(3),
                borderRadius: wp(2),
                marginVertical: hp(1),
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Text style={{color: '#292929', fontWeight: 'bold'}}>
                  {item.nama_penerima}
                </Text>
                {item.is_select === 'true' && (
                  <View
                    style={{
                      backgroundColor: '#39C5A3',
                      paddingVertical: wp(1),
                      paddingHorizontal: wp(3),
                      borderRadius: 5,
                    }}>
                    <Text style={{color: '#fff'}}>Dipilih</Text>
                  </View>
                )}
              </View>

              <Text style={{marginTop: wp(3)}}>({item.no_hp})</Text>

              <Text style={{lineHeight: 25}}>{item.alamat_lengkap}</Text>
            </TouchableOpacity>
          ))}
        </ScrollView>
        <View
          style={{
            marginHorizontal: wp(4),
            marginVertical: hp(2),
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('AddAddress')}
            style={{
              backgroundColor: '#39C5A3',
              paddingVertical: hp(1.5),
              width: wp(90),
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 7,
            }}>
            <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
              Tambah Alamat
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    // paddingBottom: hp(5),
  },
});

export default ListAddress;
