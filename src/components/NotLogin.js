import {Text, StyleSheet, View, Image, TouchableOpacity} from 'react-native';
import React, {Component} from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default class NotLogin extends Component {
  render() {
    const {onPress} = this.props;
    return (
      <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
        <Image
          source={{
            uri: 'https://i.pinimg.com/564x/ee/e2/79/eee279c7d9b4c4e5fc060a9f9bb61c5f.jpg',
          }}
          style={{width: wp(80), height: wp(80)}}
        />
        <Text
          style={{
            color: '#1E1E1E',
            fontWeight: 'bold',
            fontSize: 25,
            marginTop: hp(3),
          }}>
          Anda Belum Login!
        </Text>
        <Text style={{width: wp(70), marginTop: hp(1)}}>
          Login terlebih dahulu untuk menggunakan fitur ini
        </Text>
        <TouchableOpacity
          onPress={onPress}
          style={{
            backgroundColor: '#39C5A3',
            paddingVertical: hp(2),
            width: wp(86),
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 7,
            marginVertical: hp(6),
          }}>
          <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
            Login
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
