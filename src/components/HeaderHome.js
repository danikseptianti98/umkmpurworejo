import {Text, StyleSheet, View, TouchableOpacity, Image} from 'react-native';
import React, {Component} from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Logo from '../assets/logo.png';
export default class HeaderHome extends Component {
  render() {
    const {title, onPress} = this.props;
    return (
      <View
        style={{
          backgroundColor: '#fff',
          paddingHorizontal: wp(1),
          paddingVertical: hp(1),
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
 
       
   
        <Image
          source={Logo}
          style={{width: wp(40), height: hp(5), resizeMode: 'contain'}}
        />
           <TouchableOpacity onPress={onPress} style={{marginRight: wp(4)}}>
            <Icon name="cart" color={'#39C5A3'} size={25} />
          </TouchableOpacity>
        {/* <Text style={{color: '#292929', fontSize: 18, fontWeight: 'bold'}}>
          {title}
        </Text> */}
        {/* <TouchableOpacity>
          <Icon name="dots-horizontal" color={'#292929'} size={25} />
        </TouchableOpacity> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({});
