import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';
import React, {Component} from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
export default class HeaderTop extends Component {
  render() {
    const {title, onPress} = this.props;
    return (
      <View
        style={{
          backgroundColor: '#fff',
          paddingHorizontal: wp(4),
          paddingVertical: hp(2),
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        {onPress !== undefined && (
          <TouchableOpacity onPress={onPress}>
            <Icon name="chevron-left" color={'#292929'} size={25} />
          </TouchableOpacity>
        )}

        <Text style={{color: '#292929', fontSize: 18, fontWeight: 'bold'}}>
          {title}
        </Text>
        {/* <TouchableOpacity>
          <Icon name="dots-horizontal" color={'#292929'} size={25} />
        </TouchableOpacity> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({});
